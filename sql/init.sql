DROP TABLE IF EXISTS "public"."product" CASCADE;
DROP TABLE IF EXISTS "public"."promotion" CASCADE;

CREATE TABLE public.product
(
    id SERIAL PRIMARY KEY,
    ref VARCHAR(64) NOT NULL,
    price DOUBLE PRECISION NOT NULL
);

CREATE TABLE public.promotion
(
    id SERIAL PRIMARY KEY,
    productRef VARCHAR(64),
    valeur DOUBLE PRECISION NOT NULL
);
