package controller;

import Service.*;
import Service.CaisseService.*;
import Service.ServicesKit.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dbService.ProductService;
import model.ProductEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {
    private int instanceID = 1;
    private String appName = "caisse1";
    private String appip;
    private String stip;

    private ParseFunction fctn = null;

    public HomeController() {
        this.fctn = new ParseFunction("", "");
    }

    @RequestMapping(value = {"/", "/home"})
    public String home() {
        return "index";
    }

    @RequestMapping(value = "/api/getIp", method = RequestMethod.GET)
    public @ResponseBody String getAppIp() {
        JSONObject jo = new JSONObject();
        jo.put("appName", this.appName);
        jo.put("appip", appip);
        jo.put("stip", stip);
        jo.put("instanceId", instanceID);
        return jo.toString();
    }

    @RequestMapping(value = "/api/setappname", method = RequestMethod.POST)
    public ModelAndView setAppName(HttpServletRequest request) {
        this.appName = request.getParameter("inputappname");
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/api/setappip", method = RequestMethod.POST)
    public ModelAndView setAppIp(HttpServletRequest request) {
        this.appip = request.getParameter("inputid");
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/api/setstip", method = RequestMethod.POST)
    public ModelAndView setStIp(HttpServletRequest request) {
        this.stip = request.getParameter("inputstid");
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/api/setinstanceid", method = RequestMethod.POST)
    public ModelAndView setInstandeId(HttpServletRequest request) {
        this.instanceID = Integer.parseInt(request.getParameter("inputinstanceid"));
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/env")
    public ModelAndView setEnv(HttpServletRequest request) {
        String hostIp = System.getenv("DB_IP");
        if (hostIp == null) {
            return new ModelAndView("redirect:/");
        }
        this.appName = "CA";
        this.appip = hostIp + ":8080/CA";
        this.instanceID = 1;
        this.stip = hostIp + ":3000";
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/caisse1")
    public ModelAndView setcaisse1(HttpServletRequest request) {
        String hostIp = System.getenv("DB_IP");
        if (hostIp == null) return new ModelAndView("redirect:/");
        this.setProdEnv(hostIp, "8080", 1);
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/caisse2")
    public ModelAndView setcaisse2(HttpServletRequest request) {
        String hostIp = System.getenv("DB_IP");
        if (hostIp == null) return new ModelAndView("redirect:/");
        this.setProdEnv(hostIp, "8081", 2);
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/caisse3")
    public ModelAndView setcaisse3(HttpServletRequest request) {
        String hostIp = System.getenv("DB_IP");
        if (hostIp == null) return new ModelAndView("redirect:/");
        this.setProdEnv(hostIp, "8080", 3);
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/caisse4")
    public ModelAndView setcaisse4(HttpServletRequest request) {
        String hostIp = System.getenv("DB_IP");
        if (hostIp == null) return new ModelAndView("redirect:/");
        this.setProdEnv(hostIp, "8081", 4);
        return new ModelAndView("redirect:/");
    }

    private void setProdEnv(String hostip, String port, int instanceID) {
        this.appName = "CA";
        this.appip = hostip + ":" + port + "/CA";
        this.instanceID = instanceID;
        this.stip = hostip;
    }



    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public @ResponseBody String endConnection() throws IOException, org.json.simple.parser.ParseException {
        /**
         * Running the post method to send the data to the st
         */
        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + this.stip + "/api/end_connection"),
                new StringEntity("data=" + new Gson().toJson(new Message(appName, instanceID, new TrueData("")))));
        return myHttpPost.execute();
    }

    /**
     * Handshake function
     * @return
     * @throws IOException
     * @throws org.json.simple.parser.ParseException
     */
    @RequestMapping(value = "/api/handshake", method = RequestMethod.GET)
    public @ResponseBody String handshake() throws IOException, org.json.simple.parser.ParseException {
        String senderOut = System.getenv("AppName");
        if (senderOut == null) {
            senderOut = "caisse1";
        }
        System.out.println("Doing handshake");
        /**
         * Creating the agenda
         */
        Agenda[] agendaOut = new Agenda[3];
        agendaOut[0] = new Agenda("09:00", "5", 10,"http://" + this.appip + "/checkFile");
        agendaOut[1] = new Agenda("10:00", "5", 12,"http://" + this.appip + "/checkFile");
        agendaOut[2] = new Agenda("11:00", "5", 13,"http://" + this.appip + "/checkFile");
        Handshake handOut = new Handshake(this.appName, this.instanceID, this.appip, agendaOut);

        /**
         * Running the post method to send the data to the st
         */
        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + this.stip + "/api/handshake"),
                new StringEntity("data=" + new Gson().toJson(handOut)));

        return myHttpPost.execute();
    }

    /**
     * Get the message and send a response
     * @param fct
     * @param request
     * @return
     * @throws ParseException
     */
    @RequestMapping(value = "/api/msg", method = RequestMethod.POST)
    public @ResponseBody String getMessage(@RequestParam(required = true, value = "fct", defaultValue = "") String fct,
                                              @RequestParam(required = true, value = "data", defaultValue = "") String data,
                                              HttpServletRequest request)
            throws ParseException {
        System.out.println("=========================================================================================");
        System.out.println("/api/msg function = " + fct);

        ParseFunction parseFunction = new ParseFunction(this.appName, this.instanceID, this.appip, this.stip);
        parseFunction.setFct(fct);
        parseFunction.setData(data);

        String serviceResponse = parseFunction.execute();

        return "data=" + new Gson().toJson(serviceResponse);
    }

    /**
     * Get the service and send the result to the st
     * @param fct
     * @param request
     * @return
     * @throws ParseException
     */
    @RequestMapping(value = "/api/service", method = RequestMethod.POST)
    public @ResponseBody String getService(@RequestParam(required = true, value = "fct", defaultValue = "") String fct,
                                              @RequestParam(required = true, value = "data", defaultValue = "") String data,
                                              HttpServletRequest request)
            throws ParseException {
        System.out.println("=========================================================================================");
        System.out.println("/api/service function = " + fct);

        ParseFunction parseFunction = new ParseFunction(this.appName, this.instanceID, this.appip, this.stip);
        parseFunction.setFct(fct);
        parseFunction.setData(data);

        String serviceResponse = parseFunction.execute();

        return "data=" + new Gson().toJson(serviceResponse);
    }

    /**
     * Send file to the st
     * @param targetName
     * @param targetInstance
     * @param fctName
     * @return
     * @throws IOException
     */
    public @ResponseBody String sendFile(String targetName, String targetInstance, String fctName) throws IOException {
        SendFile target = new SendFile(instanceID, "test", this.appName);
        File fileLocation = new File("myfile"); /* FIXME */
        MyHttpPostFile myHttpPostFile = new MyHttpPostFile(new HttpPost("http://" + this.stip + "/api/send_file?fct="
                + fctName + "&target=" + targetName + "&targetInstance=" + targetInstance + "&sender=" + this.appName
                + "&senderInstance=" + this.instanceID),
                new StringEntity("data=" + new Gson().toJson(target)), fileLocation);
        return myHttpPostFile.execute();
    }

    /**
     * Get file from st and store it into the application
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/api/notif_file", method = RequestMethod.POST)
    public @ResponseBody String notifFile(@RequestParam(required = true, value = "fct", defaultValue = "") String fct,
                                        @RequestParam(required = true, value = "data", defaultValue = "") String data,
                                        HttpServletRequest request) throws IOException {
        System.out.println("notif File !!!!!!!!!");
        JSONObject jsonObj = new JSONObject(data);
        System.out.println(data);
        System.out.println(jsonObj.get("fileID"));
        String file = jsonObj.get("fileID").toString().replaceAll("\"", "")
                .replaceAll("[\\[\\]\"]", "").replaceAll(" ", "+");

//        FIXME
//        MyHttpGetFile myHttpGetFile = new MyHttpGetFile("http://" + this.stip + "/api/get_file?target=" + this.appName
//                + "&targetInstance=" + this.instanceID + "&fileID=" + file, "/project/upload-dir/123-", file,
//                Integer.toString(this.instanceID));
//        String result = myHttpGetFile.execute();

        Response response = new Response(true, "Every thing works !");
        return "data=" + new Gson().toJson(response);
    }

    /*
        TEST PART
     */

    @RequestMapping(value = "/ticket")
    public @ResponseBody String sendTicket() {
        System.out.println("Send Ticket with CASH : ");
        List<Produit> list = new ArrayList<>();

        for (int i = 1; i < 8; ++i) {
            list.add(new Produit("X-" + i, i));
        }

        Customer client = new Customer("CASH", null, null, list);
        String data = new GsonBuilder().create().toJson(new CaisseJsonHeader(this.appName, this.instanceID, client));
        String result = "";

        result = ticketSendTest(data);

        return result;
    }

    @RequestMapping(value = "/ticketCB")
    public @ResponseBody String customerCbToCA(HttpServletRequest request) {
        System.out.println("Send Ticket with CB : ");
        List<Produit> list = new ArrayList<>();

        for (int i = 1; i < 3; ++i) {
            list.add(new Produit("X-" + i, i));
        }

        Customer client = new Customer("CARD", null, "CB-1234-5435-5467", list);
        String data = new GsonBuilder().create().toJson(new CaisseJsonHeader(this.appName, this.instanceID, client));
        String result = "";

        result = ticketSendTest(data);

        return result;
    }

    @RequestMapping(value = "/ticketFID")
    public @ResponseBody String customerFidToCA(HttpServletRequest request) throws IOException {
        System.out.println("Send Ticket with FID : ");
        List<Produit> list = new ArrayList<>();

        for (int i = 1; i < 5; ++i) {
            list.add(new Produit("X-" + i, i));
        }

        Customer client = new Customer("DIFFERED", "FID-42", null, list);
        String data = new GsonBuilder().create().toJson(new CaisseJsonHeader(this.appName, this.instanceID, client));
        String result = "";

        result = ticketSendTest(data);

        return result;
    }

    private String ticketSendTest(String data) {
        MyHttpPost myHttpPost;
        String result = "";
        try {
            String url = "http://" + this.stip + "/api/service?fct=ticket&target=" + this.appName + "&targetInstance=1";
            System.out.println("/ticket send data : " + data);
            System.out.println("/ticket URL = " + url);

            myHttpPost = new MyHttpPost(new HttpPost(url), new StringEntity("data=" + data));
            result = myHttpPost.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }


    @RequestMapping(value = "/CA-BO-1")
    public @ResponseBody String ticketToBO(HttpServletRequest request) throws IOException {
        System.out.println("BACKOFFICE TEST ENVOIS MSG TICKET : ticketToBO");

        List<ReceiptProduct> basket = new ArrayList<>();
        basket.add(new ReceiptProduct("REF-0242", 2));
        basket.add(new ReceiptProduct("REF-1234", 4));
        basket.add(new ReceiptProduct("REF-5345", 1));
        basket.add(new ReceiptProduct("REF-8764", 42));

        PurchaseInfo purchaseInfo = new PurchaseInfo("0", "", 49.0, basket);
        CaisseJsonHeader boDataObject = new CaisseJsonHeader("CA", this.instanceID, purchaseInfo);
        String boData = new Gson().toJson(boDataObject);

        System.out.println("CA-BO-1 SEND DATA = " + boDataObject);

        String targetUrl = "http://" + this.stip + "/api/msg?fct=" + "ticketToBO" + "&target="
                + "BO" + "&targetInstance=" + "1";
        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost(targetUrl), new StringEntity("data=" + boData));
        return myHttpPost.execute();
    }

    @RequestMapping(value = "/test_CA-BO-1")
    public @ResponseBody String test_ticketToBO(HttpServletRequest request) throws IOException {
        System.out.println("BACKOFFICE TEST ENVOIS MSG TICKET : ticketToBO");

        List<ReceiptProduct> basket = new ArrayList<>();
        basket.add(new ReceiptProduct("REF-0242", 2));
        basket.add(new ReceiptProduct("REF-1234", 4));
        basket.add(new ReceiptProduct("REF-5345", 1));
        basket.add(new ReceiptProduct("REF-8764", 42));

        PurchaseInfo purchaseInfo = new PurchaseInfo("0", "", 49.0, basket);
        CaisseJsonHeader boDataObject = new CaisseJsonHeader("CA", this.instanceID, purchaseInfo);
        String boData = new Gson().toJson(boDataObject);

        System.out.println("CA-BO-1 SEND DATA = " + boDataObject);

        String targetUrl = "http://" + this.stip + "/api/msg?fct=" + "ticketToBO" + "&target="
                + "CA" + "&targetInstance=" + "1";
        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost(targetUrl), new StringEntity("data=" + boData));
        return myHttpPost.execute();
    }

    @RequestMapping(value = "/CA_MO_1") // Payment CARD
    public @ResponseBody String ticketCBToMO(HttpServletRequest request) throws IOException {
        System.out.println("MONETIQUE TEST CARD : ticketCBToMO");

        BankCardPayment bankCardPayment = new BankCardPayment("CB-2446-3435-2345-6543", 42.42);
        CaisseJsonHeader moDataObject = new CaisseJsonHeader("CA", this.instanceID, bankCardPayment);
        String moData = new Gson().toJson(moDataObject);

        System.out.println("CA_MO_1 SEND DATA = " + moData);

        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + this.stip + "/api/service?fct=ticketReceiptToMO&target=MO&targetInstance=1"),
                new StringEntity("data=" + moData));
        return myHttpPost.execute();
    }

    @RequestMapping(value = "/CA_MO_2") // Payment DIFFERED
    public @ResponseBody String ticketFidToMO(HttpServletRequest request) throws IOException {
        System.out.println("MONETIQUE TEST DIFFERED : ticketFidToMO");

        FidelityCardPayment fidelityCardPayment = new FidelityCardPayment("FID-87843-42345", 32.32);
        CaisseJsonHeader moDataObject = new CaisseJsonHeader("CA", this.instanceID, fidelityCardPayment);
        String moData = new Gson().toJson(moDataObject);

        System.out.println("CA_MO_2 SEND DATA = " + moData);

        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + this.stip + "/api/service?fct=ticketReceiptFidToMO&target=MO&targetInstance=1"),
                new StringEntity("data=" + moData));

        return myHttpPost.execute();
    }

    @RequestMapping(value = "/test_CA_MO_1") // Test for CA
    public @ResponseBody String test_ticketCBToMO(HttpServletRequest request) throws IOException {
        System.out.println("MONETIQUE TEST CARD : ticketCBToMO");

        BankCardPayment bankCardPayment = new BankCardPayment("CB-2446-3435-2345-6543", 42.42);
        CaisseJsonHeader moDataObject = new CaisseJsonHeader("CA", this.instanceID, bankCardPayment);
        String moData = new Gson().toJson(moDataObject);

        System.out.println("CA_MO_1 SEND DATA = " + moData);

        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + this.stip + "/api/service?fct=ticketReceiptToMO&target=CA&targetInstance=1"),
                new StringEntity("data=" + moData));
        return myHttpPost.execute();
    }

    @RequestMapping(value = "/test_CA_MO_2") // Payment DIFFERED
    public @ResponseBody String test_ticketFidToMO(HttpServletRequest request) throws IOException {
        System.out.println("MONETIQUE TEST DIFFERED : ticketFidToMO");
        FidelityCardPayment fidelityCardPayment = new FidelityCardPayment("FID-87843-42345", 32.32);
        CaisseJsonHeader moDataObject = new CaisseJsonHeader("CA", this.instanceID, fidelityCardPayment);
        String moData = new Gson().toJson(moDataObject);

        System.out.println("CA_MO_2 SEND DATA = " + moData);

        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + this.stip + "/api/service?fct=ticketReceiptFidToMO&target=CA&targetInstance=1"),
                new StringEntity("data=" + moData));

        return myHttpPost.execute();
    }


    @RequestMapping(value = "/promotionsToCaisse") // BO-CA-1 promotionsToCA
    public @ResponseBody String promotionsToCaisseMessage(HttpServletRequest request) throws IOException {
        System.out.println("Send promotions from BO to CAISSE by Msg : promotionsToCaisse");
        List<Promotion> list = new ArrayList<>();
        list.add(new Promotion("REF-01", 0.95)); // reduction de 20% si achat de 3 ou plus
        list.add(new Promotion("REF-02", 0.75)); // tout les 3 produit tu n'en paye que 1
        list.add(new Promotion("REF-03", 0.90));  // tout les 2 produits A tu as droit à une réduction de 4 euros

        ListPromotion listPromotion = new ListPromotion(list);
        String data = new GsonBuilder().create().toJson(new CaisseJsonHeader(this.appName, this.instanceID, listPromotion));

        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + this.stip + "/api/msg?fct=promotionsToCA&target=" + "CA" +"&targetInstance=" + "1" + ""),
                new StringEntity("data=" + data));

        return myHttpPost.execute();
    }

    @RequestMapping(value = "/productsToCaisse") // BO-CA-2 Simulation BO send Referentiel to CA
    public @ResponseBody String productsToCaisseMessage(HttpServletRequest request) throws IOException {
        System.out.println("Send referentiel from BO to CAISSE by Msg : productsToCaisse");
        Gson gson = new Gson();

        List<ReferenceProduct> list = new ArrayList<>();
        for (int i = 1; i < 8; ++i) {
            list.add(new ReferenceProduct("REF-0" + i, 42 + i));
        }

        list.add(new ReferenceProduct("REF-32", 32.45));
        list.add(new ReferenceProduct("REF-33", 45.67));
        list.add(new ReferenceProduct("REF-34", 29.99));
        list.add(new ReferenceProduct("REF-36", 1));
        list.add(new ReferenceProduct("REF-37", 99.99));

        ListReferenceProduct productReference = new ListReferenceProduct(list);
        String data = gson.toJson(new CaisseJsonHeader(this.appName, this.instanceID, productReference));
        System.out.println("Referentiel data = " + data);

        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + this.stip + "/api/msg?fct=productsToCA&target=" + "CA" +"&targetInstance=" + "1" + ""),
                new StringEntity("data=" + data));

        return myHttpPost.execute();
    }

    @RequestMapping(value = "/couponToCaisse") // BO-CA-3 Simulation BO send Coupon to CA
    public @ResponseBody String couponToCaisseMessage(HttpServletRequest request) throws IOException {
        System.out.println("Send coupons from BO to CAISSE by Msg : couponToCaisse");
        List<Coupon> coupons = new ArrayList<>();
        coupons.add(new Coupon("COUPON-X1", 2));
        coupons.add(new Coupon("COUPON-03", 1));
        coupons.add(new Coupon("COUPON-07", 4));
        ListCoupon listCoupon = new ListCoupon("REC-ID-0746", coupons);
        String data = new GsonBuilder().create().toJson(new CaisseJsonHeader(this.appName, this.instanceID, listCoupon));

        System.out.println("List des coupons data = " + data);
        String targetUrl = "http://" + this.stip + "/api/msg?fct=couponsToCA&target=" + this.appName +"&targetInstance=" + this.instanceID;
//        "http://192.168.1.1:8080/api/msg?fct=couponsToCA&target=BO&targetInstance=1";
        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost(targetUrl), new StringEntity("data=" + data));

        return myHttpPost.execute();
    }

    @RequestMapping(value = "/testSt") // test DB in ST
    public @ResponseBody String testSt(HttpServletRequest request) throws IOException {
        ParseFunction parseFunction = new ParseFunction(this.appName, this.instanceID, this.appip, this.stip);
        parseFunction.setFct("testSt");
        parseFunction.setData("data");

        return "testSt";
    }


    @RequestMapping(value = "/insertJCData") // test DB in ST
    public @ResponseBody String insertJCData(HttpServletRequest request) throws IOException {
        ProductService productService = new ProductService();
        for (int i = 0; i < 70; ++i) {
            System.out.println("Create entity no : " + i);
            ProductEntity productEntity = new ProductEntity("X-" + i, i);
            productService.createProduct(productEntity);
        }






        System.out.println("OKOKOK");
        return "OK";
    }


    @RequestMapping(value = "/testMsg")
    public @ResponseBody String testSendMessage(HttpServletRequest request) throws IOException {





        TrueData trueData = new TrueData("toto");
        String data = new Gson().toJson(new Message(this.appName, this.instanceID, trueData));
        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + this.stip + "/api/msg?fct=ClientToCA&target=" + this.appName +"&targetInstance=" + this.instanceID + ""),
                new StringEntity("data=" + data));
        return myHttpPost.execute();
    }

    @RequestMapping(value = "/testService")
    public @ResponseBody String testService(HttpServletRequest request) throws IOException {
        TrueData trueData = new TrueData("toto");
        String data = new Gson().toJson(new Message(this.appName, this.instanceID, trueData));
        System.out.println("this.ip = " + this.stip);
        MyHttpPost myHttpPost = new MyHttpPost(new HttpPost("http://" + this.stip + "/api/service?fct=WebService&target=" + this.appName + "&targetInstance=" + this.instanceID + ""),
                new StringEntity("data=" + data));
        return myHttpPost.execute();
    }

    @RequestMapping(value = "/testFile")
    public @ResponseBody String testSendFile() throws IOException {
        TrueData trueData = new TrueData("toto");
        String fctName = "http://" + this.appName + "/checkFile";
        String targetName = this.appName;
        String targetInstance = Integer.toString(this.instanceID);
        Message target = new Message(this.appName, this.instanceID, trueData);
        File fileLocation = new File("/project/upload-dir/hello.txt");
        MyHttpPostFile myHttpPostFile = new MyHttpPostFile(new HttpPost("http://" + this.stip + "/api/send_file?fct=" + fctName + "&target=" + targetName + "&targetInstance=" + targetInstance + "&sender=" + this.appName + "&senderInstance=" + this.instanceID),
                new StringEntity("data=" + new Gson().toJson(target)), fileLocation);
        return myHttpPostFile.execute();
    }

    @RequestMapping(value = "/checkFile")
    public @ResponseBody String checkFile() throws IOException {
        File fileLocation = new File("/project/upload-dir/hello.txt");
        System.out.println(fileLocation);
        File dir = new File("/project");
        File[] filesList = dir.listFiles();
        for (File file : filesList) {
            if (file.isDirectory()) {
                System.out.println(file.getName());
            }
        }
        return "{result : " + fileLocation.isFile() + " }";
    }


    @RequestMapping("404")
    public String handlePageNotFound(ModelMap model) {
        return "404";
    }
}
