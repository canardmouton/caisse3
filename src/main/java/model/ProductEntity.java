package model;

import javax.persistence.*;

/**
 * Created by patrickear on 22/1/2017.
 */
@Entity
@Table(name = "product", schema = "public", catalog = "caisse3")
public class ProductEntity {
    @Id
    @SequenceGenerator(name="product_id_seq",sequenceName="product_id_seq", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="product_id_seq")
    @Column(name = "id", nullable = false)
    private int id;

    private String ref;
    private double price;

    public ProductEntity(String ref, double price) {
        this.ref = ref;
        this.price = price;
    }

    public ProductEntity() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "ref", nullable = false, length = 30)
    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }


    @Column(name = "price", nullable = false, precision = 0)
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductEntity that = (ProductEntity) o;

        if (id != that.id) return false;
        if (Double.compare(that.price, price) != 0) return false;
        if (ref != null ? !ref.equals(that.ref) : that.ref != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (ref != null ? ref.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
