package Service.CaisseService;

/**
 * Created by patrickear on 14/1/2017.
 */
public class CaisseJsonHeader {
    private String sender;
    private int instanceID;
    private CaisseJson data;

    public CaisseJsonHeader(String senderIn, int instanceIDIn, CaisseJson dataIn) {
        this.sender = senderIn;
        this.instanceID = instanceIDIn;
        this.data = dataIn;
    }
}
