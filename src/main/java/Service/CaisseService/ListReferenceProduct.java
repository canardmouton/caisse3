package Service.CaisseService;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by patrickear on 16/1/2017.
 */
public class ListReferenceProduct implements CaisseJson {
    List<ReferenceProduct> listProduct;

    public ListReferenceProduct(List<ReferenceProduct> listProduct) {
        this.listProduct = listProduct;
    }

    public List<ReferenceProduct> getListProduct() { return listProduct; }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
