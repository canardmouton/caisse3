package Service.CaisseService;

import java.util.List;

/**
 * Created by patrickear on 14/1/2017.
 */
public class PurchaseInfo implements CaisseJson {
    private String receiptId;
    private String customerId;
    private double totalPrice;
    private List<ReceiptProduct> receiptProducts;
    
    public PurchaseInfo(String receiptId, String customerId, double totalPrice, List<ReceiptProduct> receiptProducts) {
        this.receiptId = receiptId;
        this.customerId = customerId;
        this.totalPrice = totalPrice;
        this.receiptProducts = receiptProducts;
    }
}
