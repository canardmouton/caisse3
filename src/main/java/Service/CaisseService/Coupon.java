package Service.CaisseService;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by patrickear on 16/1/2017.
 */
public class Coupon {
    private String type_produit;
    private int discount;

    public Coupon(String type_produit, int discount) {
        this.type_produit = type_produit;
        this.discount = discount;
    }

    public String getType_produit() {
        return type_produit;
    }

    public int getDiscount() {
        return discount;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
