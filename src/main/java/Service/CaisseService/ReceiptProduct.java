package Service.CaisseService;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by patrickear on 14/1/2017.
 */
public class ReceiptProduct {
    private String productRef;
    private int quantity;

    public ReceiptProduct(String productRef, int quantity) {
        this.productRef = productRef;
        this.quantity = quantity;
    }

    public String getProductRef() { return productRef; }

    public int getQuantity() { return quantity; }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
