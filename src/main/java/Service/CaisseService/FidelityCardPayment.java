package Service.CaisseService;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by patrickear on 21/1/2017.
 */
public class FidelityCardPayment implements CaisseJson {
    private String fidelityCard;
    private double amount;

    public FidelityCardPayment(String fidelityCard, double amount) {
        this.fidelityCard = fidelityCard;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
