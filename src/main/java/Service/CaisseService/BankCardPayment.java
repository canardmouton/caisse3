package Service.CaisseService;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by patrickear on 21/1/2017.
 */
public class BankCardPayment implements CaisseJson {
    private String creditCard;
    private double amount;

    public BankCardPayment(String creditCard, double amount) {
        this.creditCard = creditCard;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
