package dbService;

import Service.CaisseService.Promotion;
import model.PromotionEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Created by patrickear on 22/1/2017.
 */
public class PromotionService {
    private EntityManagerFactory emf;
    private EntityManager em;

    public PromotionService() {
        this.emf = Persistence.createEntityManagerFactory("caisse3");
        this.em = emf.createEntityManager();
    }

    public void createListOfProducts(List<Promotion> list) {
        System.out.println("== createListOfProducts ==");
        em.getTransaction().begin();
        for (Promotion promotion: list) {
            em.persist(new PromotionEntity(promotion.getProductRef(), promotion.getValeur()));
        }
        em.getTransaction().commit();
    }
}
